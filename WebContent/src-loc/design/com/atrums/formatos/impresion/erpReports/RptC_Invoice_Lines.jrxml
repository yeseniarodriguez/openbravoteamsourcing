<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RptC_Invoice_Lines" pageWidth="482" pageHeight="842" columnWidth="482" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="9fdd8bf6-5131-4872-b014-61f6b8ac1d68">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<style name="default" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="8"/>
	<style name="Report_Title" fontName="Bitstream Vera Sans" fontSize="18"/>
	<style name="Report_Subtitle" forecolor="#555555" fontName="Bitstream Vera Sans" fontSize="14"/>
	<style name="Report_Data_Label" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Report_Data_Field" fontName="Bitstream Vera Sans" fontSize="11" isBold="false"/>
	<style name="Total_Field" mode="Opaque" forecolor="#000000" backcolor="#CCCCCC" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="GroupHeader_DarkGray" mode="Opaque" forecolor="#FFFFFF" backcolor="#555555" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="12" isBold="true"/>
	<style name="Group_Data_Label" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Group_Data_Field" fontName="Bitstream Vera Sans" fontSize="11"/>
	<style name="Detail_Header" mode="Opaque" forecolor="#FFFFFF" backcolor="#5D5D5D" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="10" isBold="true"/>
	<style name="Detail_Line" fontName="Bitstream Vera Sans" fontSize="8">
		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()%2==0)]]></conditionExpression>
			<style mode="Opaque" backcolor="#CCCCCC"/>
		</conditionalStyle>
	</style>
	<style name="Total_Gray" mode="Opaque" forecolor="#000000" backcolor="#CCCCCC"/>
	<style name="Detail_Data_Label" mode="Opaque" backcolor="#CCCCCC" fontName="Bitstream Vera Sans" fontSize="10" isBold="true"/>
	<style name="Detail_Data_Field" mode="Opaque" backcolor="#CCCCCC" fontName="Bitstream Vera Sans" fontSize="10"/>
	<style name="Group_Footer" fontName="Bitstream Vera Sans" fontSize="11" isBold="true"/>
	<style name="Report_Footer" isDefault="true" vAlign="Middle" fontName="Bitstream Vera Sans" fontSize="11"/>
	<parameter name="C_INVOICE_ID" class="java.lang.String"/>
	<parameter name="NUMBERFORMAT" class="java.text.DecimalFormat" isForPrompting="false"/>
	<parameter name="LOCALE" class="java.util.Locale" isForPrompting="false"/>
	<parameter name="ISTAXINCLUDED" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT C_INVOICE.C_INVOICE_ID, COALESCE(M_PRODUCT.NAME, C_INVOICELINE.DESCRIPTION) AS NAME_PRODUCT, C_INVOICELINE.QTYINVOICED,
     C_UOM.NAME AS UOMNAME, C_INVOICELINE.DESCRIPTION,
        CASE WHEN $P{ISTAXINCLUDED}='Y' THEN round(C_INVOICELINE.LINE_GROSS_AMOUNT,2) ELSE round(C_INVOICELINE.LINENETAMT,2) END AS LINENETAMT,
        CASE WHEN $P{ISTAXINCLUDED}='Y' THEN round(C_INVOICELINE.GROSS_UNIT_PRICE,2) ELSE round(C_INVOICELINE.PRICEACTUAL,2) END AS PRICEACTUAL,
        M_INOUT.DOCUMENTNO AS INOUTNO, C_ORDER.DOCUMENTNO AS ORDERNO, M_PRODUCT.VALUE AS REFERENCE
        FROM C_INVOICELINE left join M_PRODUCT on C_INVOICELINE.M_PRODUCT_ID = M_PRODUCT.M_PRODUCT_ID
                           left join C_UOM on C_INVOICELINE.C_UOM_ID = C_UOM.C_UOM_ID
                           left join M_INOUTLINE on  M_INOUTLINE.M_INOUTLINE_ID = C_INVOICELINE.M_INOUTLINE_ID
                           left join M_INOUT on M_INOUTLINE.M_INOUT_ID = M_INOUT.M_INOUT_ID
                           left join C_ORDERLINE on C_ORDERLINE.C_ORDERLINE_ID = C_INVOICELINE.C_ORDERLINE_ID
                           left join C_ORDER on C_ORDERLINE.C_ORDER_ID = C_ORDER.C_ORDER_ID,
           C_INVOICE
        WHERE C_INVOICELINE.C_INVOICE_ID = C_INVOICE.C_INVOICE_ID
        AND C_INVOICE.C_INVOICE_ID = $P{C_INVOICE_ID}
        ORDER BY C_INVOICELINE.LINE]]>
	</queryString>
	<field name="C_INVOICE_ID" class="java.lang.String"/>
	<field name="NAME_PRODUCT" class="java.lang.String"/>
	<field name="QTYINVOICED" class="java.math.BigDecimal"/>
	<field name="UOMNAME" class="java.lang.String"/>
	<field name="DESCRIPTION" class="java.lang.String"/>
	<field name="LINENETAMT" class="java.math.BigDecimal"/>
	<field name="PRICEACTUAL" class="java.math.BigDecimal"/>
	<field name="INOUTNO" class="java.lang.String"/>
	<field name="ORDERNO" class="java.lang.String"/>
	<field name="REFERENCE" class="java.lang.String"/>
	<variable name="TOTAL_LINENETAMT" class="java.math.BigDecimal" resetType="Group" resetGroup="C_INVOICE_ID" calculation="Sum">
		<variableExpression><![CDATA[$F{LINENETAMT}]]></variableExpression>
	</variable>
	<group name="C_INVOICE_ID" isResetPageNumber="true">
		<groupExpression><![CDATA[$F{C_INVOICE_ID}]]></groupExpression>
		<groupHeader>
			<band height="16" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-1" style="Detail_Header" x="0" y="0" width="72" height="16" uuid="1fafa111-69e4-4075-a03c-569c51f7c4c3"/>
					<box leftPadding="5">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="false"/>
					</textElement>
					<text><![CDATA[REFERENCE]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-2" style="Detail_Header" x="72" y="0" width="140" height="16" uuid="fb3835f3-0aa2-4ff0-a037-1873ef007c7b"/>
					<box leftPadding="5">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="false"/>
					</textElement>
					<text><![CDATA[PRODUCT NAME]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-3" style="Detail_Header" x="212" y="0" width="50" height="16" uuid="3f178ff3-248d-44a5-9dda-e86c40846dff"/>
					<box leftPadding="5">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font isBold="false"/>
					</textElement>
					<text><![CDATA[UOM]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-4" style="Detail_Header" x="262" y="0" width="58" height="16" uuid="eff464dc-8e10-4b48-af28-982c5e1157e4"/>
					<box leftPadding="5">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<text><![CDATA[QUANTITY]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-8" style="Detail_Header" x="321" y="0" width="79" height="16" uuid="c43b4017-761c-4db5-bd69-6eeec11baeb0"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="false"/>
					</textElement>
					<text><![CDATA[PRICE]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-9" style="Detail_Header" x="401" y="0" width="81" height="16" uuid="062def4b-a13a-4319-96df-cafe21f56270"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="10" isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<text><![CDATA[TOTAL]]></text>
				</staticText>
				<line>
					<reportElement key="line-9" style="Report_Footer" x="400" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="583ac14d-ea71-4329-a692-67d6e21cfcd6"/>
				</line>
				<line>
					<reportElement key="line-10" style="Report_Footer" x="482" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="2aa1b25e-6f6e-4a00-8e1d-1e9f13fdd697"/>
				</line>
				<line>
					<reportElement key="line-11" style="Report_Footer" x="321" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="044c05f6-ef08-4dab-9443-699bafd77926"/>
				</line>
				<line>
					<reportElement key="line-12" style="Report_Footer" x="262" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="03f65ca5-f847-401e-8763-945c72175bb5"/>
				</line>
				<line>
					<reportElement key="line-13" style="Report_Footer" x="211" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="6f0b9550-2d52-418c-8869-b69d14cf943d"/>
				</line>
				<line>
					<reportElement key="line-14" style="Report_Footer" x="72" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="7bcdf2b3-c079-4f6e-97fb-3c2258360644"/>
				</line>
				<line>
					<reportElement key="line-15" style="Report_Footer" x="0" y="0" width="1" height="16" forecolor="#FFFFFF" uuid="1de2fdd8-ccd0-4000-951c-221ad5c23891"/>
				</line>
				<line>
					<reportElement key="line-16" style="Report_Footer" x="0" y="0" width="482" height="1" forecolor="#FFFFFF" uuid="473b7fcf-0b83-4882-9cdd-660475c75191"/>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Stretch">
				<line>
					<reportElement key="line-1" style="Report_Footer" x="0" y="0" width="482" height="1" uuid="e7c9c14e-4624-4749-9f0a-69714acc9d26"/>
				</line>
				<staticText>
					<reportElement key="staticText-7" style="default" x="262" y="1" width="138" height="16" uuid="25643bf2-ec0f-49ce-b6d8-5f5b34729102">
						<printWhenExpression><![CDATA[$P{ISTAXINCLUDED}.equals("N")]]></printWhenExpression>
					</reportElement>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font size="10"/>
					</textElement>
					<text><![CDATA[TOTAL (WITHOUT TAXES):]]></text>
				</staticText>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField" style="Total_Gray" x="400" y="1" width="81" height="16" uuid="b10dfa55-cf92-4fd9-a4f2-4c132ea08eac"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right" verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[($V{TOTAL_LINENETAMT}!=null)?$P{NUMBERFORMAT}.format($V{TOTAL_LINENETAMT}):new String(" ")]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-7" style="default" x="351" y="1" width="49" height="16" uuid="1302539e-5295-45b7-96d3-7acf8f2d88f5">
						<printWhenExpression><![CDATA[$P{ISTAXINCLUDED}.equals("Y")]]></printWhenExpression>
					</reportElement>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font size="10"/>
					</textElement>
					<text><![CDATA[TOTAL :]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="16" splitType="Stretch">
			<frame>
				<reportElement key="frame-1" style="Detail_Line" x="0" y="0" width="482" height="16" uuid="e12386f4-223b-43db-82d3-60e22ecdfae7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-1" style="default" x="0" y="0" width="72" height="16" forecolor="#000000" uuid="350879c9-4a75-4412-ab96-9afbb0903ad9"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{REFERENCE}]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-2" style="default" x="400" y="0" width="81" height="16" forecolor="#000000" uuid="a0afef41-0189-4954-94c3-461c21a73d6d"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[($F{LINENETAMT}!=null)?$P{NUMBERFORMAT}.format($F{LINENETAMT}):new String(" ")]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-3" style="default" x="320" y="0" width="79" height="16" forecolor="#000000" uuid="1be87593-ae48-497d-a92c-d855a7d309f0"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[($F{PRICEACTUAL}!=null)?$P{NUMBERFORMAT}.format($F{PRICEACTUAL}):new String(" ")]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="false">
					<reportElement key="textField-4" style="default" x="262" y="0" width="58" height="16" forecolor="#000000" uuid="c55be6fe-d341-4a02-8f2d-a86e2474d93f"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right"/>
					<textFieldExpression><![CDATA[($F{QTYINVOICED}!=null)?$P{NUMBERFORMAT}.format($F{QTYINVOICED}):new String(" ")]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-5" style="default" x="212" y="0" width="50" height="16" forecolor="#000000" uuid="4174b55a-3f89-4a39-9e56-2f305e857bdd"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textFieldExpression><![CDATA[$F{UOMNAME}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-6" style="default" x="72" y="0" width="140" height="16" forecolor="#000000" uuid="532a0123-5451-4feb-857c-5d9967971828"/>
					<box leftPadding="2" rightPadding="2">
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement verticalAlignment="Middle"/>
					<textFieldExpression><![CDATA[$F{NAME_PRODUCT}]]></textFieldExpression>
				</textField>
			</frame>
			<line>
				<reportElement key="line-2" style="Report_Footer" stretchType="RelativeToBandHeight" x="72" y="0" width="1" height="16" uuid="417fabcc-518e-40d6-b2cf-8ae726fb8277"/>
			</line>
			<line>
				<reportElement key="line-3" style="Report_Footer" stretchType="RelativeToBandHeight" x="0" y="0" width="1" height="16" uuid="70edc9c6-1ae3-4616-9833-857aae4b9646"/>
			</line>
			<line>
				<reportElement key="line-4" style="Report_Footer" stretchType="RelativeToBandHeight" x="211" y="0" width="1" height="16" uuid="2e35a5d9-34bd-4939-8a29-4a91bbd7a900"/>
			</line>
			<line>
				<reportElement key="line-5" style="Report_Footer" stretchType="RelativeToBandHeight" x="262" y="0" width="1" height="16" uuid="fc8debd6-5845-4a0c-877a-248688f7655a"/>
			</line>
			<line>
				<reportElement key="line-6" style="Report_Footer" stretchType="RelativeToBandHeight" x="321" y="0" width="1" height="16" uuid="2b38327a-fa10-4005-901b-360e88cfee1c"/>
			</line>
			<line>
				<reportElement key="line-7" style="Report_Footer" stretchType="RelativeToBandHeight" x="400" y="0" width="1" height="16" uuid="3dbe258f-6839-4646-aa4f-3154d1733a8b"/>
			</line>
			<line>
				<reportElement key="line-8" style="Report_Footer" stretchType="RelativeToBandHeight" x="482" y="0" width="1" height="16" uuid="55dd7b3f-befc-4916-b739-16b34f43b60c"/>
			</line>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
