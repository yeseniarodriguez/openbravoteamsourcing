<?xml version="1.0" encoding="UTF-8" ?>
<!--
*************************************************************************
* EL CONTENIDO DE ESTE ARCHIVO ES PROPIEDAD INTELECTUAL DE ATRUMS-IT Y ESTA
* SUJETA A LA LICENCIA: MOZILLA   PUBLIC  LICENSE
**************************************************************************
-->

<SqlClass name="ATECFEGenerarXmlData" package="com.atrums.felectronica.process">
 <SqlMethod name="methodSeleccionardummy" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select d.dummy as dato1, 
			 d.dummy as dato2, 
			 d.dummy as dato3, 
			 d.dummy as dato4,
			 d.dummy as dato5,
			 d.dummy as dato6,
			 d.dummy as dato7,
			 d.dummy as dato8,
			 d.dummy as dato9
	  from dual d 
	  where d.dummy = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
 </SqlMethod>
 
 <SqlMethod name="methodSeleccionarInvo" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select encode(em_atecfe_documento_xml,'base64') as dato1 
	  from c_invoice 
	  where c_invoice_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSelDirMatriz" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
        select coalesce(c.address1,coalesce(c.address2,'')) as dato1
          from ad_orginfo ad, ad_org og, c_location c
         where ad.ad_org_id = og.ad_org_id
           and og.issummary = 'Y'
           and og.ad_orgtype_id = '1'
           and c.c_location_id = ad.c_location_id
		   and og.ad_client_id = ?
         limit 1
      ]]>
    </Sql>
	<Parameter name="adClientId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarImpues" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select coalesce(tc.em_atecfe_codtax,'') as dato1, 
			 coalesce(t.em_atecfe_tartax,'') as dato2, 
			 round(coalesce(it.taxbaseamt,0),2) as dato3, 
			 round(coalesce(it.taxamt,0),2) as dato4, 
			 round(coalesce(t.rate,0),2) as dato5
	  from c_invoice i 
		   inner join c_invoicetax it on (i.c_invoice_id = it.c_invoice_id) 
		   inner join c_tax t on (it.c_tax_id = t.c_tax_id)
           inner join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)
	  where tc.em_atecfe_codtax is not null and i.c_invoice_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarImpuesReten" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select case when upper(crcl.tipo) = 'FUENTE' then '1'
            when upper(crcl.tipo) = 'IVA' then '2'
            when upper(crcl.tipo) = 'ISD' then '6'
			end as dato1,
			coalesce(ctr.tiporetencionfuente,coalesce(ctr.tiporetencioniva,'')) as dato2,
			coalesce(crcl.base_imp_retencion,0) as dato3,
			coalesce(ctr.porcentaje,0) as dato4,
			coalesce(crcl.valor_retencion,0) as dato5,
			'01' as dato6
		from co_retencion_compra_linea crcl 
			inner join co_bp_retencion_compra cbrc on (crcl.co_bp_retencion_compra_id = cbrc.co_bp_retencion_compra_id)
			inner join co_tipo_retencion ctr on (cbrc.co_tipo_retencion_id = ctr.co_tipo_retencion_id)
		where crcl.co_retencion_compra_id = ?
      ]]>
    </Sql>
	<Parameter name="co_retencion_compra_id"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDetalles" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select coalesce(p.value,'') as dato1, 
			 coalesce(null,'') as dato2, 
			 coalesce(p.description,coalesce(p.name,'')) as dato3,
			 round(coalesce(il.qtyinvoiced,0),2) as dato4, 
			 coalesce(il.priceactual,0) as dato5,
			 round((coalesce(il.em_atecfe_descuento,0) / 100) * (il.priceactual * il.qtyinvoiced), 2) as dato6,
			 round(coalesce(il.linenetamt,0),2) as dato7,
			 coalesce(il.c_invoiceline_id,'') as dato8
	  from c_invoiceline il
	       inner join m_product p on (il.m_product_id = p.m_product_id)
	  where il.c_invoice_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDetallesGuia" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select 
	  coalesce(p.value,'') as dato1, 
	  coalesce(p.description,coalesce(p.name,'')) as dato3, 
	  round(coalesce(mil.movementqty,0),2) as dato4 
	  from m_inoutline mil 
	  inner join m_product p on (mil.m_product_id = p.m_product_id) 
	  where mil.m_inout_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDatoSusteno" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select  ci.em_co_nro_estab || '-' || ci.em_co_nro_estab || '-' || (case when length(ci.documentno) = 9 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 8 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 7 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 6 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 5 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 4 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 3 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 2 then ci.documentno else '0' || 
	  (case when length(ci.documentno) = 1 then ci.documentno else '000000001' end) end) end) end) end) end) end) end) end) as dato1, to_char(ci.dateinvoiced, 'DD/MM/YYYY') as dato2 
	  from	c_invoice ci 
	  inner join m_inout mi on (ci.c_order_id = mi.c_order_id) 
	  where	ci.issotrx = 'Y' 
	  and mi.m_inout_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodVerificarGuia" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select mi.m_inout_id as dato1 
	  from c_invoice ci 
	  inner join c_invoiceline cil on (ci.c_invoice_id = cil.c_invoice_id)
	  inner join c_orderline col on (cil.c_orderline_id = col.c_orderline_id)
	  inner join c_order co on (col.c_order_id = co.c_order_id) 
	  inner join m_inout mi on (co.c_order_id = mi.c_order_id) 
	  where ci.c_invoice_id = ?
	  limit 1
      ]]>
    </Sql>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDetalTax" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select coalesce(tc.em_atecfe_codtax,'') as dato1, 
			 coalesce(t.em_atecfe_tartax,'') as dato2,
			 round(coalesce(t.rate, 0),2) as dato3, 
			 round(coalesce(ilt.taxbaseamt, 0),2) as dato4, 
			 round(coalesce(ilt.taxamt, 0),2) as dato5
	  from c_invoicelinetax ilt
		   inner join c_tax t on (ilt.c_tax_id = t.c_tax_id)
           inner join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)
	  where tc.em_atecfe_codtax is not null and ilt.c_invoiceline_id = ?
      ]]>
    </Sql>
	<Parameter name="cInvoicelineId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarAdicional" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select coalesce(null,'3') as dato1, 
			 coalesce(null,'Pagina web') as dato2,  
			 coalesce(d.dummy,'') as dato3,
			 coalesce(null,'rucFirmante') as dato4,  
			 coalesce(d.dummy,'') as dato5,
			 coalesce(null,'Factura') as dato6,  
			 coalesce(d.dummy,'') as dato7
	  from dual d
	  where d.dummy <> ?
      ]]>
    </Sql>
	<Parameter name="adClientId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDirec" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select (coalesce(l.address1,coalesce(l.address2,'')) || ', ' || coalesce(l.city,'') || '-' || coalesce(c.name,'')) as dato1 
	  from ad_orginfo oi 
		   inner join c_location l on (oi.c_location_id = l.c_location_id) 
		   inner join c_country c on (l.c_country_id = c.c_country_id) 
	  where oi.ad_org_id = ?
      ]]>
    </Sql>
    <Parameter name="adOrgId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDirecBodega" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select (coalesce(l.address1,'') || CASE WHEN l.address2 is null THEN '' ELSE ' - ' END || coalesce(l.address2,'') || CASE WHEN l.postal is null THEN '' ELSE ' - ' END || coalesce(l.postal,'') || CASE WHEN l.city is null THEN '' ELSE ', ' END || coalesce(l.city,'') || CASE WHEN c.name is null THEN '' ELSE ' - ' END || coalesce(c.name,'')) as dato1
	  from c_location l inner join 
		 c_country c on (l.c_country_id = c.c_country_id) inner join 
		 m_warehouse mw on (mw.c_location_id = l.c_location_id)
	  where mw.m_warehouse_id = ?
      ]]>
    </Sql>
    <Parameter name="adOrgId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarDirecDestinatario" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select (coalesce(l.address1,'') || CASE WHEN l.address2 is null THEN '' ELSE ' = ' END || coalesce(l.address2,'') || CASE WHEN l.postal is null THEN '' ELSE ' - ' END || coalesce(l.postal,'') || CASE WHEN l.city is null THEN '' ELSE ', ' END || coalesce(l.city,'') || CASE WHEN c.name is null THEN '' ELSE ' - ' END || coalesce(c.name,'')) as dato1
	  from c_location l inner join 
		c_country c on (l.c_country_id = c.c_country_id) inner join 
		c_bpartner_location cbl on (cbl.c_location_id = l.c_location_id)
	  where cbl.isshipto = 'Y' and cbl.isactive = 'Y' and cbl.c_bpartner_id = ?
      ]]>
    </Sql>
    <Parameter name="adOrgId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarFirma" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select acf.atecfe_conf_firma_id as dato1
	  from atecfe_conf_firma acf 
      join atecfe_conf_firma_lineas acfl on (acf.atecfe_conf_firma_id = acfl.atecfe_conf_firma_id)
	  where acf.isactive = 'Y'  
      and acfl.isactive = 'Y'
      and acfl.ad_user_id = ?
	  and acfl.ad_client_id = ?
      ]]>
    </Sql>
    <Parameter name="adUserId"/> 
	<Parameter name="adClientId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarConfiguracion" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select acs.direc_serv_transa as dato1, 
        acs.direc_serv_consul as dato2, 
        acs.nombre_bdd as dato3, 
        acs.puerto_bdd as dato4, 
        acs.usuario_bdd as dato5, 
        acs.password_bdd as dato6, 
        b.taxid as dato7,
		u.email as dato8,
		pc.smtpserverpassword as dato9
		from c_invoice i 
			 inner join atecfe_conf_servidor acs ON (i.ad_client_id = acs.ad_client_id)
			 left join c_bpartner b ON (i.c_bpartner_id = b.c_bpartner_id)
			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')
			 left join c_poc_configuration pc ON (i.ad_client_id = pc.ad_client_id)
		where i.c_invoice_id = ?
		limit 1
      ]]>
    </Sql>
    <Parameter name="c_invoice_id"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarConfiguracionTrasn" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select acs.direc_serv_transa as dato1
		from atecfe_conf_servidor acs
		where acs.ad_client_id = ?
		limit 1
      ]]>
    </Sql>
    <Parameter name="ad_client_id"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarConfiguracionReCompra" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select acs.direc_serv_transa as dato1, 
        acs.direc_serv_consul as dato2, 
        acs.nombre_bdd as dato3, 
        acs.puerto_bdd as dato4, 
        acs.usuario_bdd as dato5, 
        acs.password_bdd as dato6, 
        b.taxid as dato7,
		u.email as dato8,
		pc.smtpserverpassword as dato9
		from co_retencion_compra rc 
			 inner join atecfe_conf_servidor acs ON (rc.ad_client_id = acs.ad_client_id)
			 left join c_bpartner b ON (rc.c_bpartner_id = b.c_bpartner_id)
			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')
			 left join c_poc_configuration pc ON (rc.ad_client_id = pc.ad_client_id)
		where rc.co_retencion_compra_id = ? 
		limit 1
      ]]>
    </Sql>
    <Parameter name="co_retencion_compra_id"/> 
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarConfiguracionMinout" type="preparedStatement" return="multiple">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  select acs.direc_serv_transa as dato1, 
        acs.direc_serv_consul as dato2, 
        acs.nombre_bdd as dato3, 
        acs.puerto_bdd as dato4, 
        acs.usuario_bdd as dato5, 
        acs.password_bdd as dato6, 
        b.taxid as dato7,
		u.email as dato8,
		pc.smtpserverpassword as dato9
		from m_inout mi 
			 inner join atecfe_conf_servidor acs ON (mi.ad_client_id = acs.ad_client_id)
			 left join c_bpartner b ON (mi.c_bpartner_id = b.c_bpartner_id)
			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')
			 left join c_poc_configuration pc ON (mi.ad_client_id = pc.ad_client_id)
		where mi.m_inout_id = ? 
		limit 1
      ]]>
    </Sql>
    <Parameter name="m_inout_id"/> 
  </SqlMethod>
  
  <SqlMethod name="methodFacturaElect" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select d.em_atecfe_fac_elec as dato1, 
		coalesce(dt.templatelocation,'') as dato2, 
		coalesce(dt.templatefilename,'') as dato3, 
		coalesce(dt.reportfilename,'') as dato4
	from c_doctype d
         left join c_poc_doctype_template dt on (dt.c_doctype_id = d.c_doctype_id)
	where d.c_doctype_id = ?
	limit 1
	]]>
  </Sql>
  <Parameter name="c_doctype_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarEmail" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select (select array_to_string(ARRAY_AGG(u.email),',') from ad_user u where (b.c_bpartner_id = u.c_bpartner_id) and u.em_atecfe_check_email = 'Y') as dato1, 
		   replace(c.name,'''','''''') as dato2, 
		   replace(b.name,'''','''''') as dato3, 
		   cs.direc_serv_consul as dato4, 
		   b.taxid as dato5 
	from c_bpartner b
		 inner join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id)
		 inner join ad_client c ON (b.ad_client_id = c.ad_client_id)
		 inner join atecfe_conf_servidor cs ON (c.ad_client_id = cs.ad_client_id)
	where b.c_bpartner_id = ?
	limit 1
	]]>
  </Sql>
  <Parameter name="c_bpartner_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodSeleccionarProcess" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select ad_process_id as dato1
	from ad_process where upper(procedurename) = upper(?)
		and upper(name) = upper(?)
	]]>
  </Sql>
  <Parameter name="nameprocedure"/>
  <Parameter name="nameproce"/>
  </SqlMethod>
  
  <SqlMethod name="methodEjecutarProcessInvo" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select c_invoice_post(?,?) as dato1
	from dual d
	where d.dummy <> ?
	]]>
  </Sql>
  <Parameter name="ad_pinstance_id"/>
  <Parameter name="c_invoice_id"/>
  <Parameter name="ad_dummy_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodEjecutarProcessRet" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select co_ejecuta_retencion_compra(?) as dato1
	from dual d
	where d.dummy <> ?
	]]>
  </Sql>
  <Parameter name="ad_pinstance_id"/>
  <Parameter name="ad_dummy_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodEjecutarProcessInsepInpara" type="preparedStatement" return="multiple">
  <SqlMethodComment></SqlMethodComment>
  <Sql>
	<![CDATA[
	select ad_pinstance_para_insert(?, '10', 'AccionRet', 'CO', null, '0', '0', null, null) as dato1
	from dual d
	where d.dummy <> ?
	]]>
  </Sql>
  <Parameter name="ad_pinstance_id"/>
  <Parameter name="ad_dummy_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodInsertarPInst" type="preparedStatement" return="rowCount">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  Insert into ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, 
                 ad_user_id, updated, result, errormsg, ad_client_id, 
                 ad_org_id, createdby, updatedby, isactive)
      values (?, ?, ?, 'N', now(), 
                 ?, now(),'0','', ?, 
                 ?, '0', '0', 'Y');
      ]]>
    </Sql>
	<Parameter name="ad_pinstance_id"/> 
    <Parameter name="ad_process_id"/> 
    <Parameter name="record_id"/>
	<Parameter name="ad_user_id"/>
	<Parameter name="ad_client_id"/>
	<Parameter name="ad_org_id"/>
  </SqlMethod>
  
  <SqlMethod name="methodActualizarInvo" type="preparedStatement" return="rowCount">
  <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  Update c_invoice set em_atecfe_codigo_acc = ?, 
	  em_atecfe_documento_xml = decode(?,'base64'), 
	  em_atecfe_menobserror_sri = ?, 
	  em_atecfe_docaction = ?, 
	  em_atecfe_docstatus = ?, 
	  em_co_nro_aut_sri = ?, 
	  em_atecfe_fecha_autori = ?, 
	  em_co_vencimiento_aut_sri = date(?)   
	  where c_invoice_id = ?
      ]]>
    </Sql>
	<Parameter name="emAtecfeClave"/> 
    <Parameter name="emAtecfeDocumentoXml"/> 
    <Parameter name="emAtecfeMenobserror"/> 
    <Parameter name="emAtecfeDocaction"/> 
    <Parameter name="emAtecfeDocstatus"/> 
	<Parameter name="emCoNroAutSri"/> 
	<Parameter name="emAtecfeFechaAutori"/>
	<Parameter name="emAtecfeFechaAutoriz"/>
	<Parameter name="cInvoiceId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodActualizarMinout" type="preparedStatement" return="rowCount">
  <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  Update m_inout set em_atecfe_codigo_acc = ?, 
	  em_atecfe_documento_xml = decode(?,'base64'), 
	  em_atecfe_menobserror_sri = ?, 
	  em_atecfe_docaction = ?, 
	  em_atecfe_docstatus = ?, 
	  em_atecfe_nro_aut_sri = ?, 
	  em_atecfe_fecha_autori = ?  
	  where m_inout_id = ?
      ]]>
    </Sql>
	<Parameter name="emAtecfeClave"/> 
    <Parameter name="emAtecfeDocumentoXml"/> 
    <Parameter name="emAtecfeMenobserror"/> 
    <Parameter name="emAtecfeDocaction"/> 
    <Parameter name="emAtecfeDocstatus"/> 
	<Parameter name="emCoNroAutSri"/> 
	<Parameter name="emAtecfeFechaAutori"/>	
	<Parameter name="mInoutId"/> 
  </SqlMethod>
  
  <SqlMethod name="methodActualizarReten" type="preparedStatement" return="rowCount">
    <SqlMethodComment></SqlMethodComment>
    <Sql>
      <![CDATA[
	  Update co_retencion_compra set em_atecfe_codigo_acc = ?, 
	  em_atecfe_documento_xml = decode(?,'base64'), 
	  em_atecfe_menobserror_sri = ?, 
	  em_atecfe_docaction = ?, 
	  em_atecfe_docstatus = ?, 
	  no_autorizacion = ?, 
	  em_atecfe_fecha_autori = ? 
	  where co_retencion_compra_id = ?
      ]]>
    </Sql>
	<Parameter name="emAtecfeClave"/> 
    <Parameter name="emAtecfeDocumentoXml"/> 
    <Parameter name="emAtecfeMenobserror"/> 
    <Parameter name="emAtecfeDocaction"/> 
    <Parameter name="emAtecfeDocstatus"/>
	<Parameter name="noAutorizacion"/>	
	<Parameter name="emAtecfeFechaAutori"/>
	<Parameter name="coretencionid"/> 
  </SqlMethod>
</SqlClass>