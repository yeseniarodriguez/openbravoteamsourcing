/*
 ******************************************************************************
 * The contents of this file are subject to the   Compiere License  Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * You may obtain a copy of the License at http://www.compiere.org/license.html
 * Software distributed under the License is distributed on an  "AS IS"  basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 * The Original Code is                  Compiere  ERP & CRM  Business Solution
 * The Initial Developer of the Original Code is Jorg Janke  and ComPiere, Inc.
 * Portions created by Jorg Janke are Copyright (C) 1999-2001 Jorg Janke, parts
 * created by ComPiere are Copyright (C) ComPiere, Inc.;   All Rights Reserved.
 * Contributor(s): Openbravo SLU
 * Contributions are Copyright (C) 2001-2013 Openbravo S.L.U.
 ******************************************************************************
 */
package com.atrums.balance.evolutivo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;

/**
 * @author Fernando Iriazabal
 * 
 *         This one is the class in charge of the report of accounting
 */
public class AccountTree {
  private static Logger log4j = Logger.getLogger(AccountTree.class);
  private VariablesSecureApp vars;
  private ConnectionProvider conn;
  private AccountTreeData[] accountsFacts;
  private AccountTreeData[] accountsTree;
  private AccountTreeData[] reportElements;
  private String[] reportNodes;
  // Used to inform if the applySign function has reset to zero the qty values
  // or not
  private boolean resetFlag;
  // True when operandsCalculate() calls calculateTree(), and the calculateTree()
  // calls again operandsCalculte()
  private boolean recursiveOperands = false;

  /**
   * Constructor
   * 
   * @param _vars
   *          VariablesSecureApp object with the session methods.
   * @param _conn
   *          ConnectionProvider object with the connection methods.
   * @param _accountsTree
   *          Array of element values. (structure)
   * @param _accountsFacts
   *          Array of accounting facts. (data)
   * @param _reportNode
   *          String with the value of the parent element to evaluate.
   * @throws ServletException
   */
  public AccountTree(VariablesSecureApp _vars, ConnectionProvider _conn,
      AccountTreeData[] _accountsTree, AccountTreeData[] _accountsFacts, String _reportNode)
      throws ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree []");
    vars = _vars;
    conn = _conn;
    accountsTree = _accountsTree;
    accountsFacts = _accountsFacts;
    reportNodes = new String[1];
    reportNodes[0] = _reportNode;
    reportElements = updateTreeQuantitiesSign(null, 0, "D");
    // Calculating forms for every elements
    if (reportElements != null && reportElements.length > 0) {
      // forms: Array of accounts with its operands.
      // AccountTreeData[] operands = null;

      AccountTreeData[] operands = AccountTreeData.selectOperands(conn,
          Utility.getContext(conn, vars, "#User_Org", "AccountTree"),
          Utility.getContext(conn, vars, "#User_Client", "AccountTree"),
          OBDal.getInstance().get(ElementValue.class, reportNodes[0]).getAccountingElement()
              .getId());

      reportElements = calculateTree(operands, reportNodes, new Vector<Object>());
    }
  }

  /**
   * Constructor
   * 
   * @param _vars
   *          VariablesSecureApp object with the session methods.
   * @param _conn
   *          ConnectionProvider object with the connection methods.
   * @param _accountsTree
   *          Array of account's elements (elementValues).
   * @param _accountsFacts
   *          Array of all the fact accts.
   * @param _reportNodes
   *          Array with the value of the parent elements to evaluate (For example, first expenses
   *          then revenues) Objective tree.
   * @throws ServletException
   */
  public AccountTree(VariablesSecureApp _vars, ConnectionProvider _conn,
      AccountTreeData[] _accountsTree, AccountTreeData[] _accountsFacts, String[] _reportNodes)
      throws ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree []");
    vars = _vars;
    conn = _conn;
    accountsTree = _accountsTree;
    accountsFacts = _accountsFacts;
    reportNodes = _reportNodes;
    applySignAsPerParent();
    // Loading tree with new amounts, applying signs (Debit or Credit) and
    // setting the account level (1, 2, 3,...)
    reportElements = updateTreeQuantitiesSign(null, 0, "D");

    if (reportElements != null && reportElements.length > 0) {
      // Array of accounts with its operands.
      // Calculating forms for every elements
      // AccountTreeData[] operands = null;

      AccountTreeData[] operands = AccountTreeData.selectOperands(conn,
          Utility.getContext(conn, vars, "#User_Org", "AccountTree"),
          Utility.getContext(conn, vars, "#User_Client", "AccountTree"),
          OBDal.getInstance().get(ElementValue.class, reportNodes[0]).getAccountingElement()
              .getId());

      Vector<Object> vec = new Vector<Object>();
      AccountTreeData[] r;

      for (int i = 0; i < reportNodes.length; i++) {
        r = calculateTree(operands, reportNodes[i], new Vector<Object>());
        for (int j = 0; j < r.length; j++)
          vec.addElement(r[j]);
      }

      reportElements = new AccountTreeData[vec.size()];
      vec.copyInto(reportElements);
    }
  }

  /**
   * Method to get the processed accounts.
   * 
   * @return Array with the resultant accounts.
   */
  public AccountTreeData[] getAccounts() {
    return reportElements;
  }

  /**
   * Applies the sign to the quantity, according to the showValueCond field
   * 
   * @param qty
   *          BigDecimal value with the quantity to evaluate.
   * @param sign
   *          String with the showValueCond field value.
   * @param isSummary
   *          Boolean that indicates if this is a summary record.
   * @return BigDecimal with the correct sign applied.
   */
  private BigDecimal applyShowValueCond(BigDecimal qty, String sign, boolean isSummary) {
    // resetFlag will store whether the value has been truncated because of
    // showvaluecond or not
    resetFlag = false;
    BigDecimal total = BigDecimal.ZERO;
    if (isSummary && !sign.equalsIgnoreCase("A")) {
      if (sign.equalsIgnoreCase("P")) {
        if (qty.compareTo(total) > 0) {
          total = qty;
        } else {
          total = BigDecimal.ZERO;
          resetFlag = true;
        }
      } else if (sign.equalsIgnoreCase("N")) {
        if (qty.compareTo(total) < 0) {
          total = qty;
        } else {
          total = BigDecimal.ZERO;
          resetFlag = true;
        }
      } else
        total = qty;
    } else
      total = qty;
    return total;
  }

  /**
   * Update the quantity and the operation quantity fields of the element, depending on the
   * isDebitCredit field.
   * 
   * @param reportElement
   *          AccoutnTreeData object with the element information.
   * @param isDebitCredit
   *          String with the parameter to evaluate if is a Debit or Credit element.
   * @return AccountTreeData object with the new element's information.
   */
  private AccountTreeData setDataQty(AccountTreeData reportElement, String isDebitCredit) {
    if (reportElement == null || accountsFacts == null || accountsFacts.length == 0)
      return reportElement;
    for (int i = 0; i < accountsFacts.length; i++) {
      if (accountsFacts[i].id.equals(reportElement.id)) {
        if (isDebitCredit.equals("C")) {
          accountsFacts[i].qty = accountsFacts[i].qtycredit;
          accountsFacts[i].qtyRef = accountsFacts[i].qtycreditRef;
          accountsFacts[i].qtyMes1 = accountsFacts[i].qtyMes1;
          accountsFacts[i].qtyMes2 = accountsFacts[i].qtyMes2;
          accountsFacts[i].qtyMes3 = accountsFacts[i].qtyMes3;
          accountsFacts[i].qtyMes4 = accountsFacts[i].qtyMes4;
          accountsFacts[i].qtyMes5 = accountsFacts[i].qtyMes5;
          accountsFacts[i].qtyMes6 = accountsFacts[i].qtyMes6;
          accountsFacts[i].qtyMes7 = accountsFacts[i].qtyMes7;
          accountsFacts[i].qtyMes8 = accountsFacts[i].qtyMes8;
          accountsFacts[i].qtyMes9 = accountsFacts[i].qtyMes9;
          accountsFacts[i].qtyMes10 = accountsFacts[i].qtyMes10;
          accountsFacts[i].qtyMes11 = accountsFacts[i].qtyMes11;
          accountsFacts[i].qtyMes12 = accountsFacts[i].qtyMes12;
        }
        reportElement.qtyOperation = accountsFacts[i].qty;
        reportElement.qtyOperationRef = accountsFacts[i].qtyRef;
        reportElement.qtyOperationMes1 = accountsFacts[i].qtyMes1;
        reportElement.qtyOperationMes2 = accountsFacts[i].qtyMes2;
        reportElement.qtyOperationMes3 = accountsFacts[i].qtyMes3;
        reportElement.qtyOperationMes4 = accountsFacts[i].qtyMes4;
        reportElement.qtyOperationMes5 = accountsFacts[i].qtyMes5;
        reportElement.qtyOperationMes6 = accountsFacts[i].qtyMes6;
        reportElement.qtyOperationMes7 = accountsFacts[i].qtyMes7;
        reportElement.qtyOperationMes8 = accountsFacts[i].qtyMes8;
        reportElement.qtyOperationMes9 = accountsFacts[i].qtyMes9;
        reportElement.qtyOperationMes10 = accountsFacts[i].qtyMes10;
        reportElement.qtyOperationMes11 = accountsFacts[i].qtyMes11;
        reportElement.qtyOperationMes12 = accountsFacts[i].qtyMes12;
        BigDecimal bdQty = new BigDecimal(reportElement.qtyOperation);
        BigDecimal bdQtyRef = new BigDecimal(reportElement.qtyOperationRef);
        BigDecimal bdQtyMes1 = new BigDecimal(reportElement.qtyOperationMes1);
        BigDecimal bdQtyMes2 = new BigDecimal(reportElement.qtyOperationMes2);
        BigDecimal bdQtyMes3 = new BigDecimal(reportElement.qtyOperationMes3);
        BigDecimal bdQtyMes4 = new BigDecimal(reportElement.qtyOperationMes4);
        BigDecimal bdQtyMes5 = new BigDecimal(reportElement.qtyOperationMes5);
        BigDecimal bdQtyMes6 = new BigDecimal(reportElement.qtyOperationMes6);
        BigDecimal bdQtyMes7 = new BigDecimal(reportElement.qtyOperationMes7);
        BigDecimal bdQtyMes8 = new BigDecimal(reportElement.qtyOperationMes8);
        BigDecimal bdQtyMes9 = new BigDecimal(reportElement.qtyOperationMes9);
        BigDecimal bdQtyMes10 = new BigDecimal(reportElement.qtyOperationMes10);
        BigDecimal bdQtyMes11 = new BigDecimal(reportElement.qtyOperationMes11);
        BigDecimal bdQtyMes12 = new BigDecimal(reportElement.qtyOperationMes12);
        reportElement.qty = (applyShowValueCond(bdQty, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyRef = (applyShowValueCond(bdQtyRef, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes1 = (applyShowValueCond(bdQtyMes1, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes2 = (applyShowValueCond(bdQtyMes2, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes3 = (applyShowValueCond(bdQtyMes3, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes4 = (applyShowValueCond(bdQtyMes4, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes5 = (applyShowValueCond(bdQtyMes5, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes6 = (applyShowValueCond(bdQtyMes6, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes7 = (applyShowValueCond(bdQtyMes7, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes8 = (applyShowValueCond(bdQtyMes8, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes9 = (applyShowValueCond(bdQtyMes9, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes10 = (applyShowValueCond(bdQtyMes10, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes11 = (applyShowValueCond(bdQtyMes11, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        reportElement.qtyMes12 = (applyShowValueCond(bdQtyMes12, reportElement.showvaluecond,
            reportElement.issummary.equals("Y"))).toPlainString();
        break;
      }
    }
    return reportElement;
  }

  /**
   * This method updates all the Quantitie's signs of the tree. Is used by the constructor to
   * initialize the element's quantities. Also initializes the level of each account
   * 
   * @param rootElement
   *          String with the index from which to start updating.
   * @param level
   *          Integer with the level of the elements.
   * @param accountSign
   *          String with the is debit or credit value of the trunk.
   * @return Array of AccountTreeData with the updated tree.
   */
  private AccountTreeData[] updateTreeQuantitiesSign(String rootElement, int level,
      String accountSign) {
    if (accountsTree == null || accountsTree.length == 0)
      return accountsTree;
    AccountTreeData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    // if (log4j.isDebugEnabled())
    // log4j.debug("AccountTree.updateTreeQuantitiesSign() - elements: " +
    // elements.length);
    if (rootElement == null)
      rootElement = "0";
    for (int i = 0; i < accountsTree.length; i++) {
      if (accountsTree[i].parentId.equals(rootElement)) {
        // accountSign = accountsTree[i].accountsign;
        AccountTreeData[] dataChilds = updateTreeQuantitiesSign(accountsTree[i].nodeId,
            (level + 1), accountSign);
        accountsTree[i].elementLevel = Integer.toString(level);
        accountsTree[i] = setDataQty(accountsTree[i], accountsTree[i].accountsign);
        vec.addElement(accountsTree[i]);
        if (dataChilds != null && dataChilds.length > 0) {
          for (int j = 0; j < dataChilds.length; j++)
            vec.addElement(dataChilds[j]);
        }
      }
    }
    result = new AccountTreeData[vec.size()];
    vec.copyInto(result);
    return result;
  }

  /**
   * Method to know if an element has form or not.
   * 
   * @param indice
   *          String with the index of the element.
   * @param forms
   *          Array with the existing forms.
   * @return Boolean indicating if has or not form.
   */
  private boolean hasOperand(String indice, AccountTreeData[] forms) {
    if (indice == null) {
      log4j.error("AccountTree.hasForm - Missing index");
      return false;
    }
    for (int i = 0; i < forms.length; i++) {
      if (forms[i].id.equals(indice))
        return true;
    }
    return false;
  }

  /**
   * Method to calculate the values with the operands's conditions.
   * 
   * @param vecAll
   *          Vector with the evaluated tree.
   * @param operands
   *          Array with the operands.
   * @param accountId
   *          String with the index of the element to evaluate.
   * @param vecTotal
   *          Vector with the totals of the operation.
   */
  private void operandsCalculate(Vector<Object> vecAll, AccountTreeData[] operands,
      String accountId, Vector<Object> vecTotal, boolean isExactValue) {
    if (isExactValue) {
      recursiveOperands = true;
    } else {
      recursiveOperands = false;
    }
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.formsCalculate");
    if (reportElements == null || reportElements.length == 0)
      return;
    if (accountId == null) {
      log4j.error("AccountTree.formsCalculate - Missing accountId");
      return;
    }
    if (vecTotal == null)
      vecTotal = new Vector<Object>();
    if (vecTotal.size() == 0) {
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
      vecTotal.addElement("0");
    }
    BigDecimal total = new BigDecimal((String) vecTotal.elementAt(0));
    BigDecimal totalRef = new BigDecimal((String) vecTotal.elementAt(1));
    BigDecimal totalMes1 = new BigDecimal((String) vecTotal.elementAt(2));
    BigDecimal totalMes2 = new BigDecimal((String) vecTotal.elementAt(3));
    BigDecimal totalMes3 = new BigDecimal((String) vecTotal.elementAt(4));
    BigDecimal totalMes4 = new BigDecimal((String) vecTotal.elementAt(5));
    BigDecimal totalMes5 = new BigDecimal((String) vecTotal.elementAt(6));
    BigDecimal totalMes6 = new BigDecimal((String) vecTotal.elementAt(7));
    BigDecimal totalMes7 = new BigDecimal((String) vecTotal.elementAt(8));
    BigDecimal totalMes8 = new BigDecimal((String) vecTotal.elementAt(9));
    BigDecimal totalMes9 = new BigDecimal((String) vecTotal.elementAt(10));
    BigDecimal totalMes10 = new BigDecimal((String) vecTotal.elementAt(11));
    BigDecimal totalMes11 = new BigDecimal((String) vecTotal.elementAt(12));
    BigDecimal totalMes12 = new BigDecimal((String) vecTotal.elementAt(13));

    boolean encontrado = false;
    for (int i = 0; i < operands.length; i++) {
      if (operands[i].id.equals(accountId)) {
        encontrado = false;
        // There exists two options to calculate operands: run through
        // the already processed elements of the report (a) or call
        // calculateTree to obtain amount (b)
        /* (a) */
        for (int j = 0; j < vecAll.size(); j++) {
          AccountTreeData actual = (AccountTreeData) vecAll.elementAt(j);
          log4j.debug("AccountTree.formsCalculate - actual.nodeId: " + actual.nodeId
              + " - forms[i].nodeId: " + operands[i].nodeId);
          if (actual.nodeId.equals(operands[i].nodeId)) {
            encontrado = true;

            actual.qty = (applyShowValueCond(new BigDecimal(actual.qtyOperation),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyRef = (applyShowValueCond(new BigDecimal(actual.qtyOperationRef),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes1 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes1),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes2 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes2),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes3 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes3),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes4 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes4),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes5 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes5),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes6 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes6),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes7 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes7),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes8 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes8),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes9 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes9),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes10 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes10),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes11 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes11),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();
            actual.qtyMes12 = (applyShowValueCond(new BigDecimal(actual.qtyOperationMes12),
                actual.showvaluecond, actual.issummary.equals("Y"))).toPlainString();

            total = total
                .add(new BigDecimal(actual.qty).multiply(new BigDecimal(operands[i].sign)));

            totalRef = totalRef.add(new BigDecimal(actual.qtyRef).multiply(new BigDecimal(
                operands[i].sign)));

            totalMes1 = totalMes1.add(new BigDecimal(actual.qtyMes1).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes2 = totalMes2.add(new BigDecimal(actual.qtyMes2).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes3 = totalMes3.add(new BigDecimal(actual.qtyMes3).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes4 = totalMes4.add(new BigDecimal(actual.qtyMes4).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes5 = totalMes5.add(new BigDecimal(actual.qtyMes5).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes6 = totalMes6.add(new BigDecimal(actual.qtyMes6).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes7 = totalMes7.add(new BigDecimal(actual.qtyMes7).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes8 = totalMes8.add(new BigDecimal(actual.qtyMes8).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes9 = totalMes9.add(new BigDecimal(actual.qtyMes9).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes10 = totalMes10.add(new BigDecimal(actual.qtyMes10).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes11 = totalMes11.add(new BigDecimal(actual.qtyMes11).multiply(new BigDecimal(
                operands[i].sign)));
            totalMes12 = totalMes12.add(new BigDecimal(actual.qtyMes12).multiply(new BigDecimal(
                operands[i].sign)));
            break;
          }
        }
        /* (b) */if (!encontrado) {
          if (log4j.isDebugEnabled())
            log4j.debug("AccountTree.formsCalculate - C_ElementValue_ID: " + operands[i].nodeId
                + " not found");
          Vector<Object> amounts = new Vector<Object>();
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");

          calculateTree(operands, operands[i].nodeId, amounts, true, true);
          BigDecimal parcial = new BigDecimal((String) amounts.elementAt(0));
          BigDecimal parcialRef = new BigDecimal((String) amounts.elementAt(1));
          BigDecimal parcialMes1 = new BigDecimal((String) amounts.elementAt(2));
          BigDecimal parcialMes2 = new BigDecimal((String) amounts.elementAt(3));
          BigDecimal parcialMes3 = new BigDecimal((String) amounts.elementAt(4));
          BigDecimal parcialMes4 = new BigDecimal((String) amounts.elementAt(5));
          BigDecimal parcialMes5 = new BigDecimal((String) amounts.elementAt(6));
          BigDecimal parcialMes6 = new BigDecimal((String) amounts.elementAt(7));
          BigDecimal parcialMes7 = new BigDecimal((String) amounts.elementAt(8));
          BigDecimal parcialMes8 = new BigDecimal((String) amounts.elementAt(9));
          BigDecimal parcialMes9 = new BigDecimal((String) amounts.elementAt(10));
          BigDecimal parcialMes10 = new BigDecimal((String) amounts.elementAt(11));
          BigDecimal parcialMes11 = new BigDecimal((String) amounts.elementAt(12));
          BigDecimal parcialMes12 = new BigDecimal((String) amounts.elementAt(13));

          if (log4j.isDebugEnabled())
            log4j.debug("AccountTree.formsCalculate - parcial: " + parcial.toPlainString());
          parcial = parcial.multiply(new BigDecimal(operands[i].sign));
          parcialRef = parcialRef.multiply(new BigDecimal(operands[i].sign));
          parcialMes1 = parcialMes1.multiply(new BigDecimal(operands[i].sign));
          parcialMes2 = parcialMes2.multiply(new BigDecimal(operands[i].sign));
          parcialMes3 = parcialMes3.multiply(new BigDecimal(operands[i].sign));
          parcialMes4 = parcialMes4.multiply(new BigDecimal(operands[i].sign));
          parcialMes5 = parcialMes5.multiply(new BigDecimal(operands[i].sign));
          parcialMes6 = parcialMes6.multiply(new BigDecimal(operands[i].sign));
          parcialMes7 = parcialMes7.multiply(new BigDecimal(operands[i].sign));
          parcialMes8 = parcialMes8.multiply(new BigDecimal(operands[i].sign));
          parcialMes9 = parcialMes9.multiply(new BigDecimal(operands[i].sign));
          parcialMes10 = parcialMes10.multiply(new BigDecimal(operands[i].sign));
          parcialMes11 = parcialMes11.multiply(new BigDecimal(operands[i].sign));
          parcialMes12 = parcialMes12.multiply(new BigDecimal(operands[i].sign));
          if (log4j.isDebugEnabled())
            log4j.debug("AccountTree.formsCalculate - C_ElementValue_ID: " + operands[i].nodeId
                + " found with value: " + parcial + " account sign: " + operands[i].sign);
          total = total.add(parcial);
          totalRef = totalRef.add(parcialRef);
          totalMes1 = totalMes1.add(parcialMes1);
          totalMes2 = totalMes2.add(parcialMes2);
          totalMes3 = totalMes3.add(parcialMes3);
          totalMes4 = totalMes4.add(parcialMes4);
          totalMes5 = totalMes5.add(parcialMes5);
          totalMes6 = totalMes6.add(parcialMes6);
          totalMes7 = totalMes7.add(parcialMes7);
          totalMes8 = totalMes8.add(parcialMes8);
          totalMes9 = totalMes9.add(parcialMes9);
          totalMes10 = totalMes10.add(parcialMes10);
          totalMes11 = totalMes11.add(parcialMes11);
          totalMes12 = totalMes12.add(parcialMes12);

        }
      }
    }
    vecTotal.set(0, total.toPlainString());
    vecTotal.set(1, totalRef.toPlainString());
    vecTotal.set(2, totalMes1.toPlainString());
    vecTotal.set(3, totalMes2.toPlainString());
    vecTotal.set(4, totalMes3.toPlainString());
    vecTotal.set(5, totalMes4.toPlainString());
    vecTotal.set(6, totalMes5.toPlainString());
    vecTotal.set(7, totalMes6.toPlainString());
    vecTotal.set(8, totalMes7.toPlainString());
    vecTotal.set(9, totalMes8.toPlainString());
    vecTotal.set(10, totalMes9.toPlainString());
    vecTotal.set(11, totalMes10.toPlainString());
    vecTotal.set(12, totalMes11.toPlainString());
    vecTotal.set(13, totalMes12.toPlainString());
  }

  /**
   * Main method, which is called by the constructor to evaluate the tree.
   * 
   * @param forms
   *          Array with the forms.
   * @param indice
   *          Array with the start indexes.
   * @param vecTotal
   *          Vector with the accumulated totals.
   * @return Array with the new calculated tree.
   */
  private AccountTreeData[] calculateTree(AccountTreeData[] forms, String[] indice,
      Vector<Object> vecTotal) {
    return calculateTree(forms, indice, vecTotal, true, false);
  }

  /**
   * Main method, which is called by the constructor to evaluate the tree.
   * 
   * @param operands
   *          Array with the forms.
   * @param reportNode
   *          String with the index of the start element.
   * @param vecTotal
   *          Vector with the accumulated totals.
   * @return Array with the new calculated tree.
   */
  private AccountTreeData[] calculateTree(AccountTreeData[] operands, String reportNode,
      Vector<Object> vecTotal) {
    return calculateTree(operands, reportNode, vecTotal, true, false);
  }

  private boolean nodeIn(String node, String[] listOfNodes) {
    for (int i = 0; i < listOfNodes.length; i++)
      if (node.equals(listOfNodes[i]))
        return true;
    return false;
  }

  /**
   * Main method, which is called by the constructor to evaluate the tree.
   * 
   * @param operands
   *          Array with the forms.
   * @param reportNode
   *          String with the index of the start element.
   * @param vecTotal
   *          Vector with the accumulated totals.
   * @param applysign
   *          Boolean to know if the sign must be applied or not.
   * @param isExactValue
   *          Boolean auxiliar to use only for the calls from the forms calculating.
   * @return Array with the new calculated tree.
   */
  private AccountTreeData[] calculateTree(AccountTreeData[] operands, String reportNode,
      Vector<Object> vecTotal, boolean applysign, boolean isExactValue) {
    String[] i = new String[1];
    i[0] = reportNode;

    return calculateTree(operands, i, vecTotal, applysign, isExactValue);
  }

  /**
   * Main method, which is called by the constructor to evaluate the tree.
   * 
   * @param operands
   *          Array with the forms.
   * @param reportNode
   *          Array with the start indexes.
   * @param totalAmounts
   *          Vector with the accumulated totals.
   * @param applysign
   *          Boolean to know if the sign must be applied or not.
   * @param isExactValue
   *          Boolean auxiliar to use only for the calls from the forms calculating.
   * @return Array with the new calculated tree.
   */
  private AccountTreeData[] calculateTree(AccountTreeData[] operands, String[] reportNode,
      Vector<Object> totalAmounts, boolean applysign, boolean isExactValue) {
    if (reportElements == null || reportElements.length == 0)
      return reportElements;
    if (reportNode == null) {
      reportNode = new String[1];
      reportNode[0] = "0";
    }
    AccountTreeData[] result = null;
    Vector<Object> report = new Vector<Object>();
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.calculateTree() - accounts: " + reportElements.length);
    if (totalAmounts == null)
      totalAmounts = new Vector<Object>();
    if (totalAmounts.size() == 0) {
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
      totalAmounts.addElement("0");
    }
    BigDecimal total = new BigDecimal((String) totalAmounts.elementAt(0));
    BigDecimal totalRef = new BigDecimal((String) totalAmounts.elementAt(1));
    BigDecimal totalMes1 = new BigDecimal((String) totalAmounts.elementAt(2));
    BigDecimal totalMes2 = new BigDecimal((String) totalAmounts.elementAt(3));
    BigDecimal totalMes3 = new BigDecimal((String) totalAmounts.elementAt(4));
    BigDecimal totalMes4 = new BigDecimal((String) totalAmounts.elementAt(5));
    BigDecimal totalMes5 = new BigDecimal((String) totalAmounts.elementAt(6));
    BigDecimal totalMes6 = new BigDecimal((String) totalAmounts.elementAt(7));
    BigDecimal totalMes7 = new BigDecimal((String) totalAmounts.elementAt(8));
    BigDecimal totalMes8 = new BigDecimal((String) totalAmounts.elementAt(9));
    BigDecimal totalMes9 = new BigDecimal((String) totalAmounts.elementAt(10));
    BigDecimal totalMes10 = new BigDecimal((String) totalAmounts.elementAt(11));
    BigDecimal totalMes11 = new BigDecimal((String) totalAmounts.elementAt(12));
    BigDecimal totalMes12 = new BigDecimal((String) totalAmounts.elementAt(13));

    for (int i = 0; i < reportElements.length; i++) {
      if ((isExactValue && nodeIn(reportElements[i].nodeId, reportNode))
          || (!isExactValue && nodeIn(reportElements[i].parentId, reportNode))) { // modified by
        // Eduardo Argal.
        // For
        // operands calculation
        AccountTreeData[] reportElementChilds = null;
        if (reportElements[i].calculated.equals("N")) // this would
        // work if it
        // only passed
        // here once,
        // but it's
        // passing more
        // times...
        // why????
        {
          Vector<Object> amounts = new Vector<Object>();
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");
          amounts.addElement("0");

          @SuppressWarnings("unchecked")
          Vector<Object> reportAux = (Vector<Object>) report.clone();
          reportElementChilds = calculateTree(operands, reportElements[i].nodeId, amounts);
          if (reportElementChilds != null && reportElementChilds.length > 0) {
            for (int h = 0; h < reportElementChilds.length; h++) {
              reportAux.addElement(reportElementChilds[h]);
            }
          }
          if (!hasOperand(reportElements[i].nodeId, operands)) {
            BigDecimal parcial = new BigDecimal((String) amounts.elementAt(0));
            BigDecimal parcialRef = new BigDecimal((String) amounts.elementAt(1));
            BigDecimal parcialMes1 = new BigDecimal((String) amounts.elementAt(2));
            BigDecimal parcialMes2 = new BigDecimal((String) amounts.elementAt(3));
            BigDecimal parcialMes3 = new BigDecimal((String) amounts.elementAt(4));
            BigDecimal parcialMes4 = new BigDecimal((String) amounts.elementAt(5));
            BigDecimal parcialMes5 = new BigDecimal((String) amounts.elementAt(6));
            BigDecimal parcialMes6 = new BigDecimal((String) amounts.elementAt(7));
            BigDecimal parcialMes7 = new BigDecimal((String) amounts.elementAt(8));
            BigDecimal parcialMes8 = new BigDecimal((String) amounts.elementAt(9));
            BigDecimal parcialMes9 = new BigDecimal((String) amounts.elementAt(10));
            BigDecimal parcialMes10 = new BigDecimal((String) amounts.elementAt(11));
            BigDecimal parcialMes11 = new BigDecimal((String) amounts.elementAt(12));
            BigDecimal parcialMes12 = new BigDecimal((String) amounts.elementAt(13));

            reportElements[i].qtyOperation = (new BigDecimal(reportElements[i].qtyOperation)
                .add(parcial)).toPlainString();
            reportElements[i].qtyOperationRef = (new BigDecimal(reportElements[i].qtyOperationRef)
                .add(parcialRef)).toPlainString();
            reportElements[i].qtyOperationMes1 = (new BigDecimal(reportElements[i].qtyOperationMes1)
                .add(parcialMes1)).toPlainString();
            reportElements[i].qtyOperationMes2 = (new BigDecimal(reportElements[i].qtyOperationMes2)
                .add(parcialMes2)).toPlainString();
            reportElements[i].qtyOperationMes3 = (new BigDecimal(reportElements[i].qtyOperationMes3)
                .add(parcialMes3)).toPlainString();
            reportElements[i].qtyOperationMes4 = (new BigDecimal(reportElements[i].qtyOperationMes4)
                .add(parcialMes4)).toPlainString();
            reportElements[i].qtyOperationMes5 = (new BigDecimal(reportElements[i].qtyOperationMes5)
                .add(parcialMes5)).toPlainString();
            reportElements[i].qtyOperationMes6 = (new BigDecimal(reportElements[i].qtyOperationMes6)
                .add(parcialMes6)).toPlainString();
            reportElements[i].qtyOperationMes7 = (new BigDecimal(reportElements[i].qtyOperationMes7)
                .add(parcialMes7)).toPlainString();
            reportElements[i].qtyOperationMes8 = (new BigDecimal(reportElements[i].qtyOperationMes8)
                .add(parcialMes8)).toPlainString();
            reportElements[i].qtyOperationMes9 = (new BigDecimal(reportElements[i].qtyOperationMes9)
                .add(parcialMes9)).toPlainString();
            reportElements[i].qtyOperationMes10 = (new BigDecimal(
                reportElements[i].qtyOperationMes10).add(parcialMes10)).toPlainString();
            reportElements[i].qtyOperationMes11 = (new BigDecimal(
                reportElements[i].qtyOperationMes11).add(parcialMes11)).toPlainString();
            reportElements[i].qtyOperationMes12 = (new BigDecimal(
                reportElements[i].qtyOperationMes12).add(parcialMes12)).toPlainString();
            // log4j.debug("calculateTree - NothasForm - parcial:" + parcial
            // + " - resultantAccounts[i].qtyOperation:" + reportElements[i].qtyOperation
            // + " - resultantAccounts[i].nodeId:" + reportElements[i].nodeId);
          } else {
            amounts.set(0, "0");
            amounts.set(1, "0");
            amounts.set(2, "0");
            amounts.set(3, "0");
            amounts.set(4, "0");
            amounts.set(5, "0");
            amounts.set(6, "0");
            amounts.set(7, "0");
            amounts.set(8, "0");
            amounts.set(9, "0");
            amounts.set(10, "0");
            amounts.set(11, "0");
            amounts.set(12, "0");
            amounts.set(13, "0");

            operandsCalculate(reportAux, operands, reportElements[i].nodeId, amounts, isExactValue);
            BigDecimal parcial = new BigDecimal((String) amounts.elementAt(0));
            BigDecimal parcialRef = new BigDecimal((String) amounts.elementAt(1));
            BigDecimal parcialMes1 = new BigDecimal((String) amounts.elementAt(2));
            BigDecimal parcialMes2 = new BigDecimal((String) amounts.elementAt(3));
            BigDecimal parcialMes3 = new BigDecimal((String) amounts.elementAt(4));
            BigDecimal parcialMes4 = new BigDecimal((String) amounts.elementAt(5));
            BigDecimal parcialMes5 = new BigDecimal((String) amounts.elementAt(6));
            BigDecimal parcialMes6 = new BigDecimal((String) amounts.elementAt(7));
            BigDecimal parcialMes7 = new BigDecimal((String) amounts.elementAt(8));
            BigDecimal parcialMes8 = new BigDecimal((String) amounts.elementAt(9));
            BigDecimal parcialMes9 = new BigDecimal((String) amounts.elementAt(10));
            BigDecimal parcialMes10 = new BigDecimal((String) amounts.elementAt(11));
            BigDecimal parcialMes11 = new BigDecimal((String) amounts.elementAt(12));
            BigDecimal parcialMes12 = new BigDecimal((String) amounts.elementAt(13));

            reportElements[i].qtyOperation = (new BigDecimal(reportElements[i].qtyOperation)
                .add(parcial)).toPlainString();
            reportElements[i].qtyOperationRef = (new BigDecimal(reportElements[i].qtyOperationRef)
                .add(parcialRef)).toPlainString();
            reportElements[i].qtyOperationMes1 = (new BigDecimal(reportElements[i].qtyOperationMes1)
                .add(parcialMes1)).toPlainString();
            reportElements[i].qtyOperationMes2 = (new BigDecimal(reportElements[i].qtyOperationMes2)
                .add(parcialMes2)).toPlainString();
            reportElements[i].qtyOperationMes3 = (new BigDecimal(reportElements[i].qtyOperationMes3)
                .add(parcialMes3)).toPlainString();
            reportElements[i].qtyOperationMes4 = (new BigDecimal(reportElements[i].qtyOperationMes4)
                .add(parcialMes4)).toPlainString();
            reportElements[i].qtyOperationMes5 = (new BigDecimal(reportElements[i].qtyOperationMes5)
                .add(parcialMes5)).toPlainString();
            reportElements[i].qtyOperationMes6 = (new BigDecimal(reportElements[i].qtyOperationMes6)
                .add(parcialMes6)).toPlainString();
            reportElements[i].qtyOperationMes7 = (new BigDecimal(reportElements[i].qtyOperationMes7)
                .add(parcialMes7)).toPlainString();
            reportElements[i].qtyOperationMes8 = (new BigDecimal(reportElements[i].qtyOperationMes8)
                .add(parcialMes8)).toPlainString();
            reportElements[i].qtyOperationMes9 = (new BigDecimal(reportElements[i].qtyOperationMes9)
                .add(parcialMes9)).toPlainString();
            reportElements[i].qtyOperationMes10 = (new BigDecimal(
                reportElements[i].qtyOperationMes10).add(parcialMes10)).toPlainString();
            reportElements[i].qtyOperationMes11 = (new BigDecimal(
                reportElements[i].qtyOperationMes11).add(parcialMes11)).toPlainString();
            reportElements[i].qtyOperationMes12 = (new BigDecimal(
                reportElements[i].qtyOperationMes12).add(parcialMes12)).toPlainString();

            log4j.debug("calculateTree - HasForm - parcial:" + parcial
                + " - resultantAccounts[i].qtyOperation:" + reportElements[i].qtyOperation
                + " - resultantAccounts[i].nodeId:" + reportElements[i].nodeId);
          }
          // SVC show value condition
          String SVC = "";
          if (isExactValue && !recursiveOperands) {
            SVC = "A";
          } else {
            SVC = reportElements[i].showvaluecond;
          }
          reportElements[i].qty = (applyShowValueCond(
              new BigDecimal(reportElements[i].qtyOperation), SVC,
              reportElements[i].issummary.equals("Y"))).toPlainString();
          reportElements[i].qtyRef = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationRef), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes1 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes1), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes2 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes2), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes3 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes3), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes4 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes4), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes5 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes5), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes6 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes6), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes7 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes7), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes8 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes8), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes9 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes9), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes10 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes10), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes11 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes11), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          reportElements[i].qtyMes12 = (applyShowValueCond(new BigDecimal(
              reportElements[i].qtyOperationMes12), SVC, reportElements[i].issummary.equals("Y")))
              .toPlainString();
          if (resetFlag) {
            reportElements[i].svcreset = "Y";
            reportElements[i].svcresetref = "Y";
            reportElements[i].svcresetmes1 = "Y";
            reportElements[i].svcresetmes2 = "Y";
            reportElements[i].svcresetmes3 = "Y";
            reportElements[i].svcresetmes4 = "Y";
            reportElements[i].svcresetmes5 = "Y";
            reportElements[i].svcresetmes6 = "Y";
            reportElements[i].svcresetmes7 = "Y";
            reportElements[i].svcresetmes8 = "Y";
            reportElements[i].svcresetmes9 = "Y";
            reportElements[i].svcresetmes10 = "Y";
            reportElements[i].svcresetmes11 = "Y";
            reportElements[i].svcresetmes12 = "Y";
          }
          reportElements[i].calculated = "Y";
        }
        if (applysign) {
          total = total.add(new BigDecimal(reportElements[i].qty));
          totalRef = totalRef.add(new BigDecimal(reportElements[i].qtyRef));
          totalMes1 = totalMes1.add(new BigDecimal(reportElements[i].qtyMes1));
          totalMes2 = totalMes2.add(new BigDecimal(reportElements[i].qtyMes2));
          totalMes3 = totalMes3.add(new BigDecimal(reportElements[i].qtyMes3));
          totalMes4 = totalMes4.add(new BigDecimal(reportElements[i].qtyMes4));
          totalMes5 = totalMes5.add(new BigDecimal(reportElements[i].qtyMes5));
          totalMes6 = totalMes6.add(new BigDecimal(reportElements[i].qtyMes6));
          totalMes7 = totalMes7.add(new BigDecimal(reportElements[i].qtyMes7));
          totalMes8 = totalMes8.add(new BigDecimal(reportElements[i].qtyMes8));
          totalMes9 = totalMes9.add(new BigDecimal(reportElements[i].qtyMes9));
          totalMes10 = totalMes10.add(new BigDecimal(reportElements[i].qtyMes10));
          totalMes11 = totalMes11.add(new BigDecimal(reportElements[i].qtyMes11));
          totalMes12 = totalMes12.add(new BigDecimal(reportElements[i].qtyMes12));
        } else {
          total = total.add(new BigDecimal(reportElements[i].qtyOperation));
          totalRef = totalRef.add(new BigDecimal(reportElements[i].qtyOperationRef));
          totalMes1 = totalMes1.add(new BigDecimal(reportElements[i].qtyOperationMes1));
          totalMes2 = totalMes2.add(new BigDecimal(reportElements[i].qtyOperationMes2));
          totalMes3 = totalMes3.add(new BigDecimal(reportElements[i].qtyOperationMes3));
          totalMes4 = totalMes4.add(new BigDecimal(reportElements[i].qtyOperationMes4));
          totalMes5 = totalMes5.add(new BigDecimal(reportElements[i].qtyOperationMes5));
          totalMes6 = totalMes6.add(new BigDecimal(reportElements[i].qtyOperationMes6));
          totalMes7 = totalMes7.add(new BigDecimal(reportElements[i].qtyOperationMes7));
          totalMes8 = totalMes8.add(new BigDecimal(reportElements[i].qtyOperationMes8));
          totalMes9 = totalMes9.add(new BigDecimal(reportElements[i].qtyOperationMes9));
          totalMes10 = totalMes10.add(new BigDecimal(reportElements[i].qtyOperationMes10));
          totalMes11 = totalMes11.add(new BigDecimal(reportElements[i].qtyOperationMes11));
          totalMes12 = totalMes12.add(new BigDecimal(reportElements[i].qtyOperationMes12));
        }
        // If the element is not active and it has balance != 0 it must be shown otherwise, it must
        // not.
        ElementValue repElementAccount = OBDal.getInstance().get(ElementValue.class,
            reportElements[i].id);
        BigDecimal qtyOperation = new BigDecimal(reportElements[i].qtyOperation);
        if (repElementAccount.isActive() || (total.compareTo(BigDecimal.ZERO) != 0)
            || (qtyOperation.compareTo(BigDecimal.ZERO) != 0)) {
          report.addElement(reportElements[i]);
          if (reportElementChilds != null && reportElementChilds.length > 0) {
            for (int j = 0; j < reportElementChilds.length; j++)
              report.addElement(reportElementChilds[j]);
          }
        }
      }
    }
    totalAmounts.set(0, total.toPlainString());
    totalAmounts.set(1, totalRef.toPlainString());
    totalAmounts.set(2, totalMes1.toPlainString());
    totalAmounts.set(3, totalMes2.toPlainString());
    totalAmounts.set(4, totalMes3.toPlainString());
    totalAmounts.set(5, totalMes4.toPlainString());
    totalAmounts.set(6, totalMes5.toPlainString());
    totalAmounts.set(7, totalMes6.toPlainString());
    totalAmounts.set(8, totalMes7.toPlainString());
    totalAmounts.set(9, totalMes8.toPlainString());
    totalAmounts.set(10, totalMes9.toPlainString());
    totalAmounts.set(11, totalMes10.toPlainString());
    totalAmounts.set(12, totalMes11.toPlainString());
    totalAmounts.set(13, totalMes12.toPlainString());

    result = new AccountTreeData[report.size()];
    report.copyInto(result);
    return result;
  }

  /**
   * Method to make the level filter of the tree, to eliminate the levels that shouldn't be shown in
   * the report.
   * 
   * @param indice
   *          Array of indexes to evaluate.
   * @param found
   *          Boolean to know if the index has been found
   * @param strLevel
   *          String with the level.
   * @return New Array with the filter applied.
   */
  private AccountTreeData[] levelFilter(String[] indice, boolean found, String strLevel) {
    if (reportElements == null || reportElements.length == 0 || strLevel == null
        || strLevel.equals(""))
      return reportElements;
    AccountTreeData[] result = null;
    Vector<Object> vec = new Vector<Object>();
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.levelFilter() - accounts: " + reportElements.length);

    // if (indice == null) indice="0";
    if (indice == null) {
      indice = new String[1];
      indice[0] = "0";
    }
    for (int i = 0; i < reportElements.length; i++) {
      // if (resultantAccounts[i].parentId.equals(indice) && (!found ||
      // resultantAccounts[i].elementlevel.equalsIgnoreCase(strLevel))) {
      if (nodeIn(reportElements[i].parentId, indice)
          && (!found || reportElements[i].elementlevel.equalsIgnoreCase(strLevel))) {
        AccountTreeData[] dataChilds = levelFilter(reportElements[i].nodeId,
            (found || reportElements[i].elementlevel.equals(strLevel)), strLevel);
        if (isAccountLevelLower(strLevel, reportElements[i]))
          vec.addElement(reportElements[i]);
        if (dataChilds != null && dataChilds.length > 0)
          for (int j = 0; j < dataChilds.length; j++)
            if (isAccountLevelLower(strLevel, dataChilds[j]))
              vec.addElement(dataChilds[j]);
      }
    }
    result = new AccountTreeData[vec.size()];
    vec.copyInto(result);
    vec.clear();
    return result;
  }

  /**
   * Method to make the level filter of the tree, to eliminate the levels that shouldn't be shown in
   * the report.
   * 
   * @param indice
   *          String with the index to evaluate.
   * @param found
   *          Boolean to know if the index has been found
   * @param strLevel
   *          String with the level.
   * @return New Array with the filter applied.
   */
  private AccountTreeData[] levelFilter(String indice, boolean found, String strLevel) {
    String[] i = new String[1];
    i[0] = indice;
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.levelFilter1");
    return levelFilter(i, found, strLevel);
  }

  /**
   * Method to filter the complete tree to show only the desired levels.
   * 
   * @param indice
   *          Array of start indexes.
   * @param notEmptyLines
   *          Boolean to indicate if the empty lines must been removed.
   * @param strLevel
   *          String with the level.
   * @param isLevel
   *          Boolean not used.
   * @return New Array with the filtered tree.
   */
  public AccountTreeData[] filterStructure(String[] indice, boolean notEmptyLines, String strLevel,
      boolean isLevel) {
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.filterStructure() - accounts: " + reportElements.length);
    if (reportElements == null || reportElements.length == 0)
      return reportElements;
    AccountTreeData[] result = null;
    Vector<Object> vec = new Vector<Object>();

    AccountTreeData[] r = levelFilter(indice, false, strLevel);

    for (int i = 0; i < r.length; i++) {
      if (r[i].showelement.equals("Y")) {
        r[i].qty = (applyShowValueCond(new BigDecimal(r[i].qty), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyRef = (applyShowValueCond(new BigDecimal(r[i].qtyRef), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes1 = (applyShowValueCond(new BigDecimal(r[i].qtyMes1), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes2 = (applyShowValueCond(new BigDecimal(r[i].qtyMes2), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes3 = (applyShowValueCond(new BigDecimal(r[i].qtyMes3), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes4 = (applyShowValueCond(new BigDecimal(r[i].qtyMes4), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes5 = (applyShowValueCond(new BigDecimal(r[i].qtyMes5), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes6 = (applyShowValueCond(new BigDecimal(r[i].qtyMes6), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes7 = (applyShowValueCond(new BigDecimal(r[i].qtyMes7), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes8 = (applyShowValueCond(new BigDecimal(r[i].qtyMes8), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes9 = (applyShowValueCond(new BigDecimal(r[i].qtyMes9), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes10 = (applyShowValueCond(new BigDecimal(r[i].qtyMes10), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes11 = (applyShowValueCond(new BigDecimal(r[i].qtyMes11), r[i].showvaluecond, true))
            .toPlainString();
        r[i].qtyMes12 = (applyShowValueCond(new BigDecimal(r[i].qtyMes12), r[i].showvaluecond, true))
            .toPlainString();

        if ((!notEmptyLines || (new BigDecimal(r[i].qty).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyRef).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes1).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes2).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes3).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes4).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes5).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes6).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes7).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes8).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes9).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes10).compareTo(BigDecimal.ZERO) != 0
            || new BigDecimal(r[i].qtyMes11).compareTo(BigDecimal.ZERO) != 0 || new BigDecimal(
            r[i].qtyMes12).compareTo(BigDecimal.ZERO) != 0)) || "Y".equals(r[i].isalwaysshown)) {
          if ("Y".equals(r[i].isalwaysshown)) {
            r[i].qty = null;
            r[i].qtyRef = null;
            r[i].qtyMes1 = null;
            r[i].qtyMes2 = null;
            r[i].qtyMes3 = null;
            r[i].qtyMes4 = null;
            r[i].qtyMes5 = null;
            r[i].qtyMes6 = null;
            r[i].qtyMes7 = null;
            r[i].qtyMes8 = null;
            r[i].qtyMes9 = null;
            r[i].qtyMes10 = null;
            r[i].qtyMes11 = null;
            r[i].qtyMes12 = null;
          }
          vec.addElement(r[i]);
        }
      }
    }
    result = new AccountTreeData[vec.size()];
    vec.copyInto(result);
    return result;
  }

  /**
   * Not used
   * 
   * @param notEmptyLines
   * @param strLevel
   * @param isLevel
   */
  public void filter(boolean notEmptyLines, String strLevel, boolean isLevel) {
    if (log4j.isDebugEnabled())
      log4j.debug("filter");
    if (reportElements == null)
      log4j.warn("No resultant Acct");
    reportElements = filterStructure(reportNodes, notEmptyLines, strLevel, isLevel);
  }

  /**
   * Resets amounts of subaccounts which parents have been reset because of show value condition
   */
  public void filterSVC() {
    if (log4j.isDebugEnabled())
      log4j.debug("AccountTree.filterShowValueCond() - accounts: " + reportElements.length);
    if (reportElements == null || reportElements.length == 0)
      return;

    int[] levels = new int[3];
    levels[0] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCReset
    levels[1] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetRef
    levels[2] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes1
    // levels[3] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes2
    // levels[4] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes3
    // levels[5] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes4
    // levels[6] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes5
    // levels[7] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes6
    // levels[8] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes7
    // levels[9] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes8
    // levels[10] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes9
    // levels[11] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes10
    // levels[12] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes11
    // levels[13] = Integer.MAX_VALUE; // Value of the min level flaged as
    // SVCResetMes12

    for (int i = 0; i < reportElements.length; i++) {
      int level = Integer.parseInt(reportElements[i].elementLevel);
      if (reportElements[i].svcreset.equals("Y")) {
        levels[0] = Math.min(level, levels[0]);
      }
      if (reportElements[i].svcresetref.equals("Y")) {
        levels[1] = Math.min(level, levels[1]);
      }
      /*
       * if (reportElements[i].svcresetmes1.equals("Y")) { levels[2] = Math.min(level, levels[2]); }
       * if (reportElements[i].svcresetmes2.equals("Y")) { levels[3] = Math.min(level, levels[3]); }
       * if (reportElements[i].svcresetmes3.equals("Y")) { levels[4] = Math.min(level, levels[4]); }
       * if (reportElements[i].svcresetmes4.equals("Y")) { levels[5] = Math.min(level, levels[5]); }
       * if (reportElements[i].svcresetmes5.equals("Y")) { levels[6] = Math.min(level, levels[6]); }
       * if (reportElements[i].svcresetmes6.equals("Y")) { levels[7] = Math.min(level, levels[7]); }
       * if (reportElements[i].svcresetmes7.equals("Y")) { levels[8] = Math.min(level, levels[8]); }
       * if (reportElements[i].svcresetmes8.equals("Y")) { levels[9] = Math.min(level, levels[9]); }
       * if (reportElements[i].svcresetmes9.equals("Y")) { levels[10] = Math.min(level, levels[10]);
       * } if (reportElements[i].svcresetmes10.equals("Y")) { levels[11] = Math.min(level,
       * levels[11]); } if (reportElements[i].svcresetmes11.equals("Y")) { levels[12] =
       * Math.min(level, levels[12]); } if (reportElements[i].svcresetmes12.equals("Y")) {
       * levels[13] = Math.min(level, levels[13]); }
       */
      if (level > levels[0]) {
        reportElements[i].qty = "0.0";
      }
      if (level == levels[0] && reportElements[i].svcreset.equals("N")) {
        levels[0] = Integer.MAX_VALUE;
      }
      if (level > levels[1]) {
        reportElements[i].qtyRef = "0.0";
      }
      if (level == levels[1] && reportElements[i].svcresetref.equals("N")) {
        levels[1] = Integer.MAX_VALUE;
      }
      /*
       * if (level > levels[2]) { reportElements[i].qtyMes1 = "0.0"; } if (level == levels[2] &&
       * reportElements[i].svcresetmes1.equals("N")) { levels[2] = Integer.MAX_VALUE; }
       */
    }
  }

  private boolean isAccountLevelLower(String reportAccountLevel, AccountTreeData accountToBeAdded) {
    if (reportAccountLevel.equalsIgnoreCase("D"))
      if (accountToBeAdded.elementlevel.equalsIgnoreCase("S"))
        return false;

    return true;
  }

  private void applySignAsPerParent() {
    if (accountsTree == null || accountsTree.length == 0) {
      return;
    }
    String parentId = accountsTree[0].id;
    String accountSign = accountsTree[0].accountsign;
    HashMap<String, String> parentSigns = new HashMap<String, String>();
    parentSigns.put(parentId, accountSign);
    for (AccountTreeData node : accountsTree) {
      parentId = node.id;
      accountSign = node.accountsign;
      parentSigns.put(parentId, accountSign);
      if (parentSigns.get(node.parentId) != null
          && !node.accountsign.equals(parentSigns.get(node.parentId))) {
        node.accountsign = parentSigns.get(node.parentId);
      }
    }
    return;
  }

}
